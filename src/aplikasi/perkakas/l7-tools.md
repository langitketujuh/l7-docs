# L7 Tools

## Deskripsi

[L7 Tools] merupakan perkakas cli untuk melakukan konfigurasi sistem seperti pembaruan, chroot mode, memasang grub, memperbaiki boot order, memasang pengguna baru dan lain-lain.

- Memperbarui sistem agar menjadi lebih baru.

  ```
  doas sky --upgrade
  ```

  Atau menjalankan

  ```
  upgrade
  ```

- Memasang grub.

  ```
  sky --grub
  ```

- Masuk ke mode chroot, berguna untuk memperbaiki jika ada kernel panic atau masalah lainnya.

  ```
  sky --chroot
  ```

- Memperbaiki screen tearing dan menampilakan grub menu sistem operasi lain.

  ```
  sky --patch
  ```

- Memasang pengguna baru, berguna untuk mengatasi gagal login.

  ```
  sky --user
  ```

- Menghapus perangkat lunak yang tidak dibutuhkan.
  ```
  sky --downgrade
  ```

Untuk menjalankan perintah diatas memerlukan akses root (doas/sudo). Lebih jelasnya jalankan `sky --help`.
  ```
  ➜  ~ sky --help

  sky <version>
  Configuring tool for upgrade, chroot and grub installer.

  license : GPL-3.0-or-later
  usage   : sky [option]
  option  :
            --chroot      -c    enter chroot mode
            --downgrade   -d    downgrade & uninstall program
            --grub        -g    install grub
            --patch       -p    reinstall patch
            --remote      -r    terminal remote
            --upgrade     -u    upgrade system
            --user        -m    create new user
            --help        -h    show this help
            --version     -v    show sky version
  ```

[L7 Tools]:https://gitlab.com/langitketujuh/l7-tools/
